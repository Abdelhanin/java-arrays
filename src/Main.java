import java.util.Arrays;
import java.util.Scanner;

public class Main {
    // Create a program using arrays that sorts a list of integers in descending order.
    // Descending order is highest value to lowest.
    // In other words if the array had the values in it 106, 26, 81, 5, 15 your program should
    // ultimately have an array with 106,81,26, 15, 5 in it.
    // Set up the program so that the numbers to sort are read in from the keyboard.
    // Implement the following methods - getIntegers, printArray, and sortIntegers
    // getIntegers returns an array of entered integers from keyboard
    // printArray prints out the contents of the array
    // and sortIntegers should sort the array and return a new array containing the sorted numbers
    // you will have to figure out how to copy the array elements from the passed array into a new
    // array and sort them and return the new sorted array.

    private final static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {

     /*   int[] myIntArray = new int[5];
        int[] anotherArray = myIntArray;
        myIntArray[0] = 5;
        anotherArray[1] = 7;
        System.out.println("MyIntArray  " + Arrays.toString(myIntArray));
        System.out.println("AnotherArray  " + Arrays.toString(anotherArray));*/


        System.out.println(" How many element want in the array");
        int count = scanner.nextInt();

        int[] myIntegers = getIntegers(count);

        for(int i=0; i< myIntegers.length; i++){
            System.out.println("Element "+ i + ", typed value was " + myIntegers[i]);
        }

        System.out.println("The average is: " +  getAverage(myIntegers));

        int[] sortedArr = sortIntegers(myIntegers);
        printArray(sortedArr);

        System.out.println("The minimum value on the Array is : " +findMin(sortedArr));
        System.out.println("Reversed array is : " + Arrays.toString(reverse(sortedArr)));

    }


    public static int[] getIntegers(int number){
        System.out.println("Enter "+ number + " integer values. ");
        int[] values = new int[number];
        for (int i=0; i < values.length; i++) {
            int k = i + 1;
            System.out.println("Enter the " + k + " number.");
            values[i] = scanner.nextInt();
        }
        return values;
    }

    public static double getAverage(int[] array) {
        int sum = 0;
        for (int j : array) {
            sum += j;
        }

        return (double) sum / (double) array.length;
    }

    public static void printArray(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            System.out.println("Element " + i + ", has a value of " + arr[i]);
        }
    }

    public static int[] sortIntegers(int[] array) {
//        int[] sortedArray = new int[array.length];
//        System.arraycopy(array, 0, sortedArray, 0, array.length);
        int[] sortedArray = Arrays.copyOf(array, array.length);

        boolean flag = true;
        int temp;
        while (flag){
            flag = false;
            for (int i = 0; i < sortedArray.length - 1; i++) {
                if (sortedArray[i] < sortedArray[i+1]) {
                    temp = sortedArray[i];
                    sortedArray[i] = sortedArray[i+1];
                    sortedArray[i+1] = temp;
                    flag = true;
                }
            }
        }
        return sortedArray;
    }

    public static int findMin(int[] array) {
        int minValue = Integer.MAX_VALUE;
        for(int i=1;i<array.length;i++){
            if(array[i] < minValue){
                minValue = array[i];
            }
        }
        return minValue;
    }

    public static int[] reverse(int[] array) {
        int maxIndex = array.length - 1;
        int halfLength = array.length / 2;
        for (int i = 0; i < halfLength; i++) {
            int temp = array[i];
            array[i] = array[maxIndex - i];
            array[maxIndex - i] = temp;
        }
        return array;
    }

}
